/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.models;

import com.datastax.driver.mapping.annotations.ClusteringColumn;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;

/**
 *
 * @author onelove
 */
@Table(keyspace = "sensors", name = "sensor_list", readConsistency = "QUORUM",
        writeConsistency = "QUORUM", caseSensitiveKeyspace = false,
        caseSensitiveTable = false)
public class Sensor_List {

    @PartitionKey(0)
    @Column(name = "sensor_id")
    private String sensor_id;
    @Column(name = "region")
    private String region;
    @PartitionKey(1)
    @Column(name = "model")
    private String model;
    private String event_json;
    @ClusteringColumn
    private long ts;

    public Sensor_List(String sensor_id, String region, String model) {
        this.sensor_id = sensor_id;
        this.region = region;
        this.model = model;
    }

    public Sensor_List(String sensor_id, String region, String model, String event_json, long ts) {
        this.sensor_id = sensor_id;
        this.region = region;
        this.model = model;
        this.event_json = event_json;
        this.ts = ts;
    }
    
    public void setSensor_id(String sensor_id) {
        this.sensor_id = sensor_id;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setEvent_json(String event_json) {
        this.event_json = event_json;
    }

    //Setters
    public void setTs(long ts) {    
        this.ts = ts;
    }

    // Getters
    public String getSensor_id() {
        return sensor_id;
    }

    public String getRegion() {
        return region;
    }

    public String getModel() {
        return model;
    }

    public String getEvent_json() {
        return event_json;
    }

    public long getTs() {
        return ts;
    }
    
    
}
