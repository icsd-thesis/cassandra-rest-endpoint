/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 *
 * @author onelove
 */
public class Live_Data {

    private String time_to_add;
    private List<Sensor_Template> records;

    public Live_Data(String time_to_add) {
        this.time_to_add = time_to_add;
        this.records = new ArrayList();
    }

    public Live_Data() {
        this.time_to_add = "";
        this.records = new ArrayList();
    }

    public String getTime_to_add() {
        return time_to_add;
    }

    public List<Sensor_Template> getRecords() {
        return records;
    }

    public void setTime_to_add(String time_to_add) {
        this.time_to_add = time_to_add;
    }

    public void setRecords(List<Sensor_Template> records) {
        this.records = records;
    }

    public void append_to_sensor_list(Sensor_Template template) {
        this.records.add(template);
    }

}
