/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.system_static.templates;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import java.math.BigInteger;

/**
 *
 * @author onelove
 */
public class Sensor_Template {

    private long mesurments;
    private String sensor_id;
    private String date;
    private String region;
    private String model;
    private double average;
    private long ts;

    public Sensor_Template(String sensor_id, String region, String model, double average) {
        this.sensor_id = sensor_id;
        this.model = model;
        this.region = region;
        this.average = average;
    }

    public Sensor_Template(ResultSet set) {
        for (Row row : set) {
            this.mesurments = row.getLong("mesurments");
            this.sensor_id = row.getString("sensor_id");
            this.date = row.getString("date");
            this.region = row.getString("region");
            this.model = row.getString("model");
            this.average = row.getFloat("avg");
            this.ts = row.getLong("ts");
        }
    }

    public Sensor_Template(long mesurments, String sensor_id, String date, String region, String model, double average) {
        this.mesurments = mesurments;
        this.sensor_id = sensor_id;
        this.date = date;
        this.region = region;
        this.model = model;
        this.average = average;
    }

    public Sensor_Template(long mesurments, long ts, String sensor_id, String date, String region, String model, double average) {
        this.mesurments = mesurments;
        this.sensor_id = sensor_id;
        this.date = date;
        this.region = region;
        this.model = model;
        this.average = average;
        this.ts = ts;
    }

    public Sensor_Template(long mesurments, String sensor_id, String region, String model) {
        this.mesurments = mesurments;
        this.sensor_id = sensor_id;
        this.model = model;
        this.region = region;
    }

    //custom
    public final void callMethodByName(String name, String att) {
//count(*) as mesurments , sensor_id , date , region , model , AVG(value) as average
        switch (name) {
            case "average": {
                setAverage(Double.parseDouble(att));
                break;
            }
            case "date": {
                setDate(new String(att));
                break;
            }
            case "mesurments": {
                setMesurments(Integer.parseInt(att));
                break;
            }
            case "region": {
                setRegion(new String(att));
                break;
            }
            case "model": {
                setModel(new String(att));
                break;
            }
            case "sensor_id": {
                setSensor_id(new String(att));
                break;
            }
        }
    }
    //setters

    public void setTs(long ts) {
        this.ts = ts;
    }

    public void setMesurments(long mesurments) {
        this.mesurments = mesurments;
    }

    public void setSensor_id(String sensor_id) {
        this.sensor_id = sensor_id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    //Getters
    public long getTs() {
        return ts;
    }

    public long getMesurments() {
        return mesurments;
    }

    public String getSensor_id() {
        return sensor_id;
    }

    public String getDate() {
        return date;
    }

    public String getRegion() {
        return region;
    }

    public String getModel() {
        return model;
    }

    public double getAverage() {
        return average;
    }
}
