/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers.interfaces;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.icsd.cass.models.SensorPerDay;

/**
 *
 * @author onelove
 */
@Accessor
public interface SensorPerDayInterface {

    @Query("SELECT * FROM sensor_per_date")
    public Result<SensorPerDay> getAllDay();

    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_date  GROUP BY sensor_id , date ;")
    public ResultSet get_day_avg_();
//    
//    @Query("select count(*) as mesurments, sensor_id , date , region , model , AVG(value) as avg from sensor_per_date where sensor_id =:sensor and date = toDate(now()) GROP BY sensor_id , date ;")
//    public ResultSet getAvgBySensorId(@Param("sensor") String sensor);

    @Query("select count(*) as mesurments, sensor_id , date , region , model , MAX(value) as avg from sensor_per_date GROUP BY sensor_id , date ;")
    public ResultSet get_day_max_();

    @Query("select count(*) as mesurments, sensor_id , model, region  from sensor_per_date GROUP BY sensor_id , date ;")
    public ResultSet get_all_sensors();

//restrict ts
    @Query("select count(*) as mesurments,region, model, sensor_id, date,ts, AVG(value) as avg from sensor_per_date where date>= :start and date<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_average_between_ts_grooped(@Param("start") String start, @Param("finish") String finish);

    @Query("select count(*) as mesurments,region, model, sensor_id, date,ts, MIN(value) as avg from sensor_per_date where date>= :start and date<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_min_between_ts_grooped(@Param("start") String start, @Param("finish") String finish);

    @Query("select count(*) as mesurments,region, model, sensor_id, date,ts, MAX(value) as avg from sensor_per_date where date>= :start and date<= :finish GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_max_between_ts_grooped(@Param("start") String start, @Param("finish") String finish);

    @Query("select sensor_id, date,ts,region, model, value as avg from sensor_per_date where date>= :start and date<= :finish Allow Filtering;")
    public ResultSet return_values_between_ts_grooped(@Param("start") String start, @Param("finish") String finish);

    //restrict ts and sensor_id
    @Query("select count(*) as mesurments, sensor_id, date,ts, region, model, AVG(value) as avg from sensor_per_date where date>= :start and date<= :finish and sensor_id = :sensor GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_average_between_ts_grooped(@Param("sensor") String sensor, @Param("start") String start, @Param("finish") String finish);

    @Query("select count(*) as mesurments, sensor_id, date,ts, region, model, MIN(value) as avg from sensor_per_date where date>= :start and date<= :finish and sensor_id = :sensor GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_min_between_ts_grooped(@Param("sensor") String sensor, @Param("start") String start, @Param("finish") String finish);

    @Query("select count(*) as mesurments, sensor_id, date,ts, region, model, MAX(value) as avg from sensor_per_date where date>= :start and date<= :finish and sensor_id = :sensor GROUP BY sensor_id , date Allow Filtering;")
    public ResultSet return_max_between_ts_grooped(@Param("sensor") String sensor, @Param("start") String start, @Param("finish") String finish);

    @Query("select sensor_id, date,ts,region, model, value as avg from sensor_per_date where date>= :start and date<= :finish and sensor_id = :sensor Allow Filtering;")
    public ResultSet return_values_between_ts_grooped(@Param("sensor") String sensor, @Param("start") String start, @Param("finish") String finish);

//    @Query("Delete from sensor_per_date where sensor_id = :sensor and date= :date ;")
//    public void delteBySensorId(@Param("sensor") String sensor, @Param("date") String date);
//    @Query("Delete from sensor_per_date where ts>= :start and ts<= :finish and sensor_id = :sensor ;")
//    public ResultSet deletebetweenTs(@Param("sensor") String sensor, @Param("start") String start, @Param("finish") String finish);
//    
//    @Query("Delete from sensor_per_date where ts= :timestamp ")
//    public ResultSet deletebetweenTs(@Param("timestamp") String timestamp);
}
