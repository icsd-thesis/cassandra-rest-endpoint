/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers.interfaces;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;
import com.icsd.cass.models.Sensor_List;

/**
 *
 * @author onelove
 */
@Accessor
public interface SensorListInterface {
    @Query("SELECT * FROM sensor_list")
    public Result<Sensor_List> get_all_data();

    @Query("select count(*) as mesurments, sensor_id , region , model from sensor_list GROUP BY sensor_id , model ;")
    public ResultSet get_sensor_list();
}
