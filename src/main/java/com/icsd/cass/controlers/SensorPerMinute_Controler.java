/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.controlers;

import com.datastax.driver.mapping.Result;
import com.icsd.cass.controlers.interfaces.SensorPerMinuteInterface;
import com.icsd.cass.models.SensorPerMinute;
import com.icsd.cass.cassandra_drivers.Connector;
import com.icsd.cass.system_static.templates.ChartData;
import java.util.List;
import com.icsd.cass.system_static.templates.Responce_Wraper;
import com.icsd.cass.system_static.templates.Search_Template;
import com.icsd.cass.system_static.templates.Sensor_Template;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author onelove
 */
public class SensorPerMinute_Controler {

    private final static String table_name = "sensor_per_minute";
    private Connector conn = null;
    private SensorPerMinuteInterface sensors_accesor = null;

    public SensorPerMinute_Controler() {
        conn = new Connector();
        sensors_accesor = Connector.getManager().createAccessor(SensorPerMinuteInterface.class);
    }

    public List<SensorPerMinute> getAllRecords() {
        return sensors_accesor.getAll().all();
    }

    public Responce_Wraper get_Currnet_average_Records() {
        return new Responce_Wraper(sensors_accesor.get_avg_curr());
    }

    public List<SensorPerMinute> get_sensor_raw(String sensor_id) {
        return functionCutValues(sensor_id, sensors_accesor.getAll().all());
    }

    public Responce_Wraper get_Currnet_min_Records() {
        return new Responce_Wraper(sensors_accesor.get_min_curr());
    }

    public Responce_Wraper get_Currnet_max_Records() {
        return new Responce_Wraper(sensors_accesor.get_max_curr());
    }

    public Sensor_Template get_Currnet_average_Records_by_Sensor_id(String sensor_id) {
        return new Sensor_Template(sensors_accesor.get_avg_by_sensor_id_live(sensor_id));
    }

    public Responce_Wraper get_Currnet_min_Records_by_Sensor_id(String sensor_id) {
        return new Responce_Wraper(sensors_accesor.get_min_by_sensor_id_live(sensor_id));
    }

    public Responce_Wraper get_Currnet_max_Records_by_Sensor_id(String sensor_id) {
        return new Responce_Wraper(sensors_accesor.get_max_by_sensor_id_live(sensor_id));
    }

    public Responce_Wraper get_all_sensors() {
        return new Responce_Wraper(sensors_accesor.get_all_sensors(), true);
    }

    public Responce_Wraper m_searchNvisualize(Search_Template template) throws ParseException {
        return decode_request_live(template);
    }

    private Responce_Wraper decode_request_live(Search_Template template) throws ParseException {
        Responce_Wraper temp = new Responce_Wraper(sensors_accesor.getAll());
        return form_data_for_chart_live(template.getSensor_list(), temp);
    }

    private Responce_Wraper form_data_for_chart_live(List<String> keep, Responce_Wraper resp) {
        Responce_Wraper temo = new Responce_Wraper();
        HashMap<String, ChartData> data = new HashMap<>();
        for (int i = 0; i < keep.size(); i++) {
            data.put(keep.get(i), resp.getChart_data().get(keep.get(i)));
        }
        temo.setChart_data(data);
        temo.setDateList(new ArrayList<>(resp.getDateList()));
        return temo;
    }

    public void setConn(Connector conn) {
        this.conn = conn;
    }

    public void setSensors_accesor(SensorPerMinuteInterface sensors_accesor) {
        this.sensors_accesor = sensors_accesor;
    }

    public static String getTabble_name() {
        return table_name;
    }

    public Connector getConn() {
        return conn;
    }

    public SensorPerMinuteInterface getSensors_accesor() {
        return sensors_accesor;
    }

    private List<SensorPerMinute> functionCutValues(String sensor_id, List<SensorPerMinute> list) {
        List<SensorPerMinute> temp = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSensorId().equalsIgnoreCase(sensor_id)) {
                temp.add(list.get(i));
            } else {
                continue;
            }
        }
        return temp;
    }

}
