/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.testing.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.icsd.cass.cassandra_drivers.Connector;
import com.icsd.cass.conf.Config;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import static org.mockito.Matchers.eq;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.stubbing.OngoingStubbing;

/**
 *
 * @author onelove
 */
public class SensorPerRowTest {

    public SensorPerRowTest() {
    }

    @Before
    public void setUp() {
    }

    @Test
    public void testMockExample() throws Exception {
        Session session = Mockito.mock(Session.class);

        // Mock a ResultSet that gets returned from ResultSetFuture#get()
        ResultSet result = Mockito.mock(ResultSet.class);
        List<Row> rows = new ArrayList<Row>();
        Row r = Mockito.mock(Row.class);
        Mockito.doReturn(100).when(r).getInt(0);
        rows.add(r);

        Mockito.doReturn(rows).when(result).all();

        // Mock a ResultSetFuture that gets returned from Session#executeAsync()
        ResultSetFuture future = Mockito.mock(ResultSetFuture.class);
        Mockito.doReturn(result).when(future).get();
        Mockito.doReturn(future).when(session).executeAsync(Mockito.anyString());

        // Execute the query and print the 0th column of the first result.
        ResultSetFuture resultF = session.executeAsync("select * from sensor_per_day where sensor_id= 'Alcohol_Kitchen' Allow Filtering;");
        assertEquals(false, result.all().isEmpty());
        assertEquals(100, resultF.get().all().iterator().next().getInt(0));
    }

    private ResultSet mockResultSet(List<Integer> rows) throws SQLException {
        ResultSet rs = mock(ResultSet.class);

        OngoingStubbing rsNextStub = when(rs.all());
        for (Integer i : rows) {
            // We must reassign!
            rsNextStub = rsNextStub.thenReturn(true);
        }
        rsNextStub.thenReturn(false);

        OngoingStubbing rsGetIntStub = when(rs.one());
        for (Integer i : rows) {
            // We must reassign!
            rsGetIntStub = rsGetIntStub.thenReturn(i);
        }

        return rs;
    }

    protected ResultSet mockResultSet(Row... rows) {
        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.all()).thenReturn(Arrays.asList(rows));
        return mockResultSet;
    }

    protected Row mockRow(String sensor_id) {
        Row mockRow = mock(Row.class, sensor_id);
        when(mockRow.getString(eq("sensor_id"))).thenReturn(sensor_id);
        return mockRow;
    }

    
}
