/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icsd.cass.testing.repository;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

/**
 *
 * @author onelove
 */
public class SensorPerDayTest {

    public SensorPerDayTest() {
    }

    @Before
    public void setUp() {
    }

    @Test
    public void testMockExample() throws Exception {
        Session session = Mockito.mock(Session.class);

        // Mock a ResultSet that gets returned from ResultSetFuture#get()
        ResultSet result = Mockito.mock(ResultSet.class);
        List<Row> rows = new ArrayList<Row>();
        Row r = Mockito.mock(Row.class);
        Mockito.doReturn(100).when(r).getInt(0);
        rows.add(r);

        Mockito.doReturn(rows).when(result).all();

        // Mock a ResultSetFuture that gets returned from Session#executeAsync()
        ResultSetFuture future = Mockito.mock(ResultSetFuture.class);
        Mockito.doReturn(result).when(future).get();
        Mockito.doReturn(future).when(session).executeAsync(Mockito.anyString());

        // Execute the query and print the 0th column of the first result.
        ResultSetFuture resultF = session.executeAsync("select * from sensor_per_day where sensor_id= 'Alcohol_Kitchen' Allow Filtering;");
        assertEquals(false, result.all().isEmpty());
        assertEquals(100, resultF.get().all().iterator().next().getInt(0));
    }
}
