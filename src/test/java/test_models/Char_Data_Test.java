/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_models;

import com.icsd.cass.system_static.templates.ChartData;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author onelove
 */
public class Char_Data_Test {

    private HashMap<String, ChartData> chart_data;
    private List<String> dateList;

    public HashMap<String, ChartData> getChart_data() {
        return chart_data;
    }

    public List<String> getDateList() {
        return dateList;
    }

    public void setChart_data(HashMap<String, ChartData> chart_data) {
        this.chart_data = chart_data;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public Char_Data_Test() {
    }

    public Char_Data_Test(HashMap<String, ChartData> chart_data, List<String> dateList) {
        this.chart_data = chart_data;
        this.dateList = dateList;
    }

    public Char_Data_Test(List<String> dateList) {
        this.dateList = dateList;
    }

    public Char_Data_Test(HashMap<String, ChartData> chart_data) {
        this.chart_data = chart_data;
    }

    
}
