/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_models;

import com.icsd.cass.system_static.templates.ChartData;
import com.icsd.cass.system_static.templates.Sensor_Template;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author onelove
 */
public class Responce_Model {

//    private HashMap<String, ChartData> chart_data;
    private List<String> dateList;
    private ArrayList<Sensor_Template> record_list;

    public Responce_Model() {
    }

 

    public Responce_Model(List<String> dateList, ArrayList<Sensor_Template> record_list) {
        this.dateList = dateList;
        this.record_list = record_list;
    }

    public Responce_Model(ArrayList<Sensor_Template> record_list) {
        this.record_list = record_list;
    }



    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public void setRecord_list(ArrayList<Sensor_Template> record_list) {
        this.record_list = record_list;
    }



    public List<String> getDateList() {
        return dateList;
    }

    public ArrayList<Sensor_Template> getRecord_list() {
        return record_list;
    }

}
